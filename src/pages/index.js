import React from 'react';

import Layout from '../components/Layout';

// import { Link } from 'gatsby';
import Sidebar from '../components/Sidebar';
import config from '../../config';


// $(document).keydown(function(event){
//   if(event.keyCode==123){
//   return false;//Prevent from F12
//   }
//   else if(event.ctrlKey && event.shiftKey && event.keyCode==73){
//   return false; //Prevent from ctrl+shift+i
//   }
//   });
  


const IndexPage = () => (

  // document.addEventListener('keydown', function() {
  //   if (event.keyCode == 123) {
  //     return false;
  //   } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
  //     return false;
  //   } else if (event.ctrlKey && event.keyCode == 85) {
  //     return false;
  //   }
  // }, false);
  
  // if (document.addEventListener) {
  //   document.addEventListener('contextmenu', function(e) {
  //     e.preventDefault();
  //   }, false);
  // } else {
  //   document.attachEvent('oncontextmenu', function() {
  //     window.event.returnValue = false;
  //   });
  // }
  
  <Layout>
    <Sidebar />
    <div className="container-fluid p-0">
      <section
        className="resume-section p-3 p-lg-5 d-flex align-items-center"
        id="about"
      >
        <div className="w-100">
          <h1 className="mb-0">
            {config.firstName}
            <span className="text-primary">{config.lastName}</span>
          </h1>
          <div className="subheading mb-5">
            {config.address} · {config.phone} ·
            <a href={`mailto:${config.email}`}>{config.email}</a>
          </div>
          <p className="lead mb-5">
          In pursuit of a progressive career would like to establish my knowledge and skills towards the growth of an organization. Acquire knowledge, observe surroundings, and learn an experience. <br /> <br />
          
          Translate designs or wireframes into Quality, reusable code supports across browsers, API Integration, Mobile Responsive Design. Worked in Github, Bitbucket, and GitLab and knowledge about Proof of Concept, Agile Development. Implementation of Contentful CMS. Deployment with Surge.
          </p>
          {/* <p className="lead mb-5">
           
          </p> */}
          <div className="social-icons">
            {config.socialLinks.map(social => {
              const { icon, url } = social;
              return (
                <a key={url} href={url}>
                  <i className={`fab ${icon}`}></i>
                </a>
              );
            })}
          </div>
        </div>
      </section>

      <hr className="m-0" />

      <section
        className="resume-section p-3 p-lg-5 d-flex justify-content-center"
        id="experience"
      >
        <div className="w-100">
          <h2 className="mb-5">Experiences</h2>

          <div className="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
            <div className="resume-content">
              <h3 className="mb-0">UI | Front End Developer</h3>
              <div className="subheading mb-3">Tech Mahindra </div>
              <div>
              Location - Chennai, TamilNadu, India</div>
              <p>
              Building reusable components and front-end libraries for future use.
              </p>
             
             <p>User Management - Create/Update/Delete User using React Js,Material UI(React), Scss.</p>
             <p>Role Management - Create/Update/Disable Role using React Js, Scss.</p>
              <p>
              Power Company and Reports table - API integration with an advanced filter in tables using React Js, Scss.
              </p>
              <p>
              Approve, Reject Poles from wire centers based on single/multi-select values from the table using - HTML5, Javascript, Jquery.
              </p>
            </div>
            <div className="resume-date text-md-right">
              <span className="text-primary">May 2021 - Present</span>
            </div>
          </div>

          <div className="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
            <div className="resume-content">
              <h3 className="mb-0">UI Developer</h3>
              <div className="subheading mb-3">Code Bucket Solutions </div>
              <div>
                Location - Kochi, Kerela, India</div>
            
              <p>
                Developed responsive user interface components using React Js, Scss, Reactstrap, Material UI, Css3, HTML5.
              </p>
              <p>
                knowledge about Gatsby Js with GraphQL for single page web application and recharts &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;( React & D3 ).
              </p>
              <p>
                Translating designs and wireframes into Quality code, supports across browsers, Integration of API.
              </p>
            </div>
            <div className="resume-date text-md-right">
              <span className="text-primary">Nov 2020 - April 2021</span>
            </div>
          </div>

          <div className="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
            <div className="resume-content">
              <h3 className="mb-0">Junior Software Developer</h3>
              <div className="subheading mb-3">Alpha Business Solutions </div>
              <div>
              Location - Tirunelveli, TamilNadu, India</div>
              <p>
                Exploring Technology for Web Applications - React Js, Gatsby Js, Sass, Mongo DB, and Node Js. Basics in Redux.
              </p>
              <p>
                Knowledge about Wordpress Design and Modified Styles of the website - Css3.
              </p>
            </div>
            <div className="resume-date text-md-right">
              <span className="text-primary">Jan 2019 - July 2020</span>
            </div>
          </div>

          <div className="resume-item d-flex flex-column flex-md-row justify-content-between">
            <div className="resume-content">
              <h3 className="mb-0">Intern</h3>
              <div className="subheading mb-3">Agira Technologies</div>
              <div>
              Location - Chennai, TamilNadu, India</div>
              <p>
                Passion with Web Designing, Created Social media Static Profiles, templates with Html5, CSS3, and Bootstrap. Designed Social media posts using Canva and Photoshop.
              </p>
              <p>
                Web Scrapping using Python &amp; Basics in BlockChain & Crypto Trading Concept.
              </p>
              <p>
                Exploring in and out possibilities of
                Social Media Marketing, SEO strategy, Lead Generation, Quora Writer, and utilized Digital Marketing tools.
              </p>

            </div>
            <div className="resume-date text-md-right">
              <span className="text-primary">July 2018 – December 2018</span>
            </div>
          </div>
        </div>
      </section>

      <hr className="m-0" />

      {/* ------------------               Projects          --------------------------*/}

      <section
        className="resume-section p-3 p-lg-5 d-flex align-items-center"
        id="projects"
      >
        <div className="w-100">
          <h2 className="mb-5">Projects</h2>
          <ul className="fa-ul mb-0">
            <li>
              {/* <i className="fa-li fa fa-trophy text-warning"></i> */}
              <i className="fas fa-check-circle"></i>
              <span style={{ marginLeft: "7px" }}>Immersive Trails | UI Developer&nbsp;
                {/* Duration - 1 Month */}
                - Tour Based Ecommerce Project. (React, Scss, Css3) - </span>
                <a href="http://immersive.codebuckets.in/" style={{fontWeight:"600"}}> Live URL </a>
            </li>

            <li style={{ marginTop: "5px" }}>
              <i className="fas fa-check-circle"></i>
              <span style={{ marginLeft: "7px" }}>
                Customer Labs | UI Developer&nbsp;
                {/* Duration - 6 Months */}
                - Developed the front end Web application to track, responsive, reusable code and worked in
Storybook used for documenting and testing UI.(React, Scss, Css3)</span>
            </li>

            <li style={{ marginTop: "5px" }}>
              <i className="fas fa-check-circle"></i>
              <span style={{ marginLeft: "7px" }}>Template Design | UI Developer&nbsp;
                {/* Duration - 2 Weeks */}
                - The template has an elegant design that can be edited with the Theme, Color variations,
Responsive template. Developed landing pages, blogs with pagination, and search options. (Gatsby, GraphQL, Contentful CMS) - </span>
            <a href="http://talentoacademy.in/" style={{fontWeight:"600"}}> Live URL </a>
            </li>

            <li>
              {/* <i className="fa-li fa fa-trophy text-warning"></i> */}
              <i className="fas fa-check-circle"></i>
              <span style={{ marginLeft: "7px" }}>Aster CMI&nbsp;
                {/* Duration - 1 Month */}
                - Translating designs into Quality, reusable code supports across browsers, API Integration, Mobile Responsive Design. (React, Scss, Css3) - </span>
                <a href="http://astercmi.surge.sh/" style={{fontWeight:"600"}}> Live URL </a>
            </li>

            <li>
              {/* <i className="fa-li fa fa-trophy text-warning"></i> */}
              <i className="fas fa-check-circle"></i>
              <span style={{ marginLeft: "7px" }}>Formee Education | Front End Developer&nbsp;
               
                - Education Management Outsourcing Project. (React)</span>
            </li>
            <li style={{ marginTop: "5px" }}>
              <i className="fas fa-check-circle"></i>
              <span style={{ marginLeft: "7px" }}>My Org | Web Developer&nbsp;
               
                - Project Management and Employees Report Management. (Angular,Mongo DB and Node)</span>
            </li>

            <li className="list-inline-item" style={{ marginTop: "5px" }}>
              <i className="fas fa-check-circle"></i>
              <span style={{ marginLeft: "7px" }}>Geo Spatial | Junior Software Developer&nbsp;
                {/* Duration - 2 Months */}
                - Developed GIS Data application used to store, view and filter the farmer details.(React, Firebase, and SCSS) - </span>
                <a href="https://geo-spatial-new.firebaseapp.com/user-login" style={{fontWeight:"600"}}> Live URL </a>
            </li>


            <li style={{ marginTop: "5px" }}>
              <i className="fas fa-check-circle"></i>
              <span style={{ marginLeft: "7px" }}>Nellai Offers | WebDesigner&nbsp;
                - Designed E-commerce Flower Website - </span>
                
                <a href="http://nellaioffers.com/" style={{fontWeight:"600"}}> Live URL </a>
            </li>
          </ul>
        </div>
      </section>

      <hr className="m-0" />

      {/* ------------------               Skills          --------------------------*/}
      <section
        className="resume-section p-3 p-lg-5 d-flex align-items-center"
        id="skills"
      >
        <div className="w-100">
          <h2 className="mb-5">Skills</h2>

          <div className="subheading mb-3">
            Programming Languages
          </div>
          <ul className="list-inline dev-icons">
            <li className="list-inline-item" style={{fontSize:"27px"}}>
              *) React - <i className="fab fa-react"></i>
            </li>

            <li className="list-inline-item" style={{fontSize:"27px"}}>
              Scss - <i className="fab fa-sass"></i>
            </li>

            <li className="list-inline-item" style={{fontSize:"27px"}}>
              Javascript - <i className="fab fa-js-square"></i>
            </li>

            <li className="list-inline-item" style={{fontSize:"27px"}}>
              Css3 - <i className="fab fa-css3-alt"></i>
            </li>

            <li className="list-inline-item" style={{fontSize:"27px"}}>
              HTML5 - <i className="fab fa-html5"></i>
            </li>
            <li className="list-inline-item" style={{fontSize:"24px"}}>
              *) Gatsby with GraphQL implemented in Contentful CMS
            </li>
          </ul>

          <div className="subheading mb-3">Workflow</div>
          <ul className="fa-ul mb-0">
            <li>
              <i className="fa-li fa fa-check"></i>
              Mobile Responsive Design
            </li>
            <li>
              <i className="fa-li fa fa-check"></i>
              Debugging
            </li>
            <li>
              <i className="fa-li fa fa-check"></i>
              Worked in Github, Bitbucket and GitLab
            </li>
            <li>
              <i className="fa-li fa fa-check"></i>
              Proof of Concept
            </li>
            <li>
              <i className="fa-li fa fa-check"></i>
              Agile Development
            </li>
            <li>
              <i className="fa-li fa fa-check"></i>
                Implementation of Contentful CMS
            </li>
            <li>
              <i className="fa-li fa fa-check"></i>
                Deployment with Surge
            </li>
          </ul>
        </div>
      </section>

      <hr className="m-0" />

      {/* ------------------               Education          --------------------------*/}

      <section
        className="resume-section p-3 p-lg-5 d-flex align-items-center"
        id="education"
      >
        <div className="w-100">
          <h2 className="mb-5">Education</h2>

          <div className="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
            <div className="resume-content">
              <h3 className="mb-0">RAMCO INSTITUTE OF TECHNOLOGY, RAJAPALAYAM </h3>
              <div className="subheading mb-3">Bachelor of Engineering</div>
              <div>Computer Science and Engineering</div>
            </div>
            <div className="resume-date text-md-right">
              <span className="text-primary">2014 - 2018</span>
            </div>
          </div>

          <div className="resume-item d-flex flex-column flex-md-row justify-content-between">
            <div className="resume-content">
              <h3 className="mb-0">St. Xaviers Matric Hr. Sec. School, Tirunelveli</h3>
              <div className="subheading mb-3">Higher Secondary</div>
            </div>
            <div className="resume-date text-md-right">
              <span className="text-primary">2014</span>
            </div>
          </div>

          <div className="resume-item d-flex flex-column flex-md-row justify-content-between">
            <div className="resume-content">
              <h3 className="mb-0">St. Xaviers Matric Hr. Sec. School, Tirunelveli</h3>
              <div className="subheading mb-3">SSLC</div>
            </div>
            <div className="resume-date text-md-right">
              <span className="text-primary">2012</span>
            </div>
          </div>


        </div>
      </section>

      <hr className="m-0" />

      <section
        className="resume-section p-3 p-lg-5 d-flex align-items-center"
        id="interests"
      >
        <div className="w-100">
          <h2 className="mb-5">Hobby</h2>
          <p>
            Acquiring knowledge through Newspaper, inshorts, and Quora.
          </p>
          <p>
            Watching Movies, Playing Games.
          </p>
        </div>
      </section>

      {/* <hr className="m-0" /> */}


    </div>
  </Layout>
);

export default IndexPage;
