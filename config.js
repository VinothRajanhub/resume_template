module.exports = {
  siteTitle: 'Vinoth Rajan Resume', // <title>
  manifestName: 'Resume',
  manifestShortName: 'Landing', // max 12 characters
  manifestStartUrl: '/',
  manifestBackgroundColor: '#663399',
  manifestThemeColor: '#663399',
  manifestDisplay: 'standalone',
  manifestIcon: 'src/assets/img/website-icon.png',
  pathPrefix: `/gatsby-starter-resume/`, // This path is subpath of your hosting https://domain/portfolio
  firstName: 'Vinoth',
  lastName: 'Rajan',
  // social
  socialLinks: [
    {
      icon: 'fa-github',
      name: 'Github',
      url: 'https://github.com/VinothRajanhub',
    },
    {
      icon: 'fa-bitbucket',
      name: 'BitBucket',
      url: 'https://bitbucket.org/VinothRajanhub',
    },
    {
      icon: 'fa-linkedin-in',
      name: 'Linkedin',
      url: 'https://www.linkedin.com/in/vinothrajan/',
    },
    {
      icon: 'fa-quora',
      name: 'Quora',
      url: 'https://www.quora.com/profile/Vinoth-Rajan-22',
    }
  ],
  email: 'vinothrajan7642@gmail.com',
  address: 'Tirunelveli'
};
